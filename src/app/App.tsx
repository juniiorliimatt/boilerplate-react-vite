import { BrowserRouter } from 'react-router-dom';
import { MyRoutes } from './routes';

export const App = () => {
  return (
    <BrowserRouter>
      <MyRoutes />
    </BrowserRouter>
  );
};
